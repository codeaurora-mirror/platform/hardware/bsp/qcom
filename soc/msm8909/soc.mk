#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := hardware/bsp/qcom/soc/msm8909

# Arm32 device.
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a
TARGET_CPU_VARIANT := generic
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_KERNEL_ARCH := $(TARGET_ARCH)

TARGET_NO_BOOTLOADER := false
TARGET_NO_KERNEL := false

BOARD_KERNEL_CMDLINE := console=ttyHSL0,115200,n8 androidboot.console=ttyHSL0 androidboot.hardware=msm8909 msm_rtb.filter=0x237 ehci-hcd.park=3 androidboot.bootdevice=7824900.sdhci lpm_levels.sleep_disabled=1 earlyprintk androidboot.selinux=enforcing

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_FLASH_BLOCK_SIZE := 131072

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.msm8909.rc:root/init.msm8909.rc \
	system/core/rootdir/init.usb.rc:root/init.usb.rc \
	system/core/rootdir/ueventd.rc:root/ueventd.rc \

# TODO: Move common/prebuilts/sepolicy include to audio_prebuilts.mk
BOARD_SEPOLICY_DIRS += \
	$(LOCAL_PATH)/sepolicy \
	$(LOCAL_PATH)/prebuilts/sepolicy \

# Set up the local kernel.
TARGET_KERNEL_SRC := hardware/bsp/kernel/qcom/qcom-msm-3.10
ifeq ($(TARGET_KERNEL_DEFCONFIG),)
    ifeq ($(TARGET_BUILD_VARIANT),user)
        TARGET_KERNEL_DEFCONFIG := msm8909-perf_defconfig
    else
        TARGET_KERNEL_DEFCONFIG := msm8909_defconfig
    endif
endif

$(call add_kernel_configs, $(realpath $(LOCAL_PATH)/soc.kconf))

# Default Keystore HAL.
DEVICE_PACKAGES += \
	keystore.default

# Include Qualcomm Bool Control HAL.
DEVICE_PACKAGES += \
	bootctrl.$(soc_name)

# Audio feature flags.
BOARD_USES_ALSA_AUDIO := true
AUDIO_FEATURE_ENABLED_EXTN_RESAMPLER := true
AUDIO_FEATURE_ENABLED_HFP := true
AUDIO_FEATURE_ENABLED_MULTI_VOICE_SESSIONS := true
AUDIO_FEATURE_ENABLED_VOICE_CONCURRENCY := true
AUDIO_FEATURE_ENABLED_WFD_CONCURRENCY := true
BOARD_USES_SRS_TRUEMEDIA := true
AUDIO_FEATURE_ENABLED_DS2_DOLBY_DAP := true
AUDIO_FEATURE_ENABLED_ACDB_LICENSE := true
DOLBY_DAP_HW_QDSP_HAL_API := true
DOLBY_UDC_MULTICHANNEL_PCM_OFFLOAD := false
MM_AUDIO_ENABLED_FTM := true
MM_AUDIO_ENABLED_SAFX := true
TARGET_USES_QCOM_MM_AUDIO := true

# Include Qualcomm Audio HAL implementation.
DEVICE_PACKAGES += \
	audio.primary.$(soc_name)

# Install device-specific audio policy, audio effects config, media codecs and mixer path files.
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/audio-config/audio_policy.conf:system/etc/audio_policy.conf \
	$(LOCAL_PATH)/audio-config/audio_effects.conf:system/etc/audio_effects.conf \
	$(LOCAL_PATH)/audio-config/mixer_paths.xml:system/etc/mixer_paths.xml \
	$(LOCAL_PATH)/audio-config/media_codecs_8909.xml:system/etc/media_codecs.xml

# Include prebuilts to detect audio devices.
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/prebuilts/audio.rc:system/etc/init/audio.rc \

#Install pre built BSP if present.
PRODUCT_LIBRARY_PATH := $(TOP)/vendor/bsp/qcom/device/$(PRODUCT_DEVICE)/board_support_package
SYSTEM_BINS    :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/bin/*)
SYSTEM_LIBS    :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/lib/*.so)
ETC_FIRMWARE   :=  $(wildcard $(PRODUCT_LIBRARY_PATH)/etc/firmware/*)

#System bins.
PRODUCT_COPY_FILES += $(foreach file,$(SYSTEM_BINS),\
        $(file):/system/bin/$(notdir $(file))) \

#system libs.
PRODUCT_COPY_FILES += $(foreach file,$(SYSTEM_LIBS),\
        $(file):/system/lib/$(notdir $(file))) \

#etc firmware.
PRODUCT_COPY_FILES += $(foreach file,$(ETC_FIRMWARE),\
        $(file):/system/etc/firmware/$(notdir $(file))) \

