# Copyright (c) 2016, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/system/bin/sh
# Set up the firewall rules for concam-webserver

BINPATH=/system/bin

# IPv4 only rules.
iptables_webserver_ctrl_if_setup() {
  ${BINPATH}/iptables -A INPUT -p tcp --dport 4000 -j ACCEPT -w
}

iptables_live555_setup() {
  ${BINPATH}/iptables -A INPUT -p udp --dport 6970 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p udp --dport 6971 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p udp --destination 228.67.43.91 --dport 15947 -j ACCEPT -w
}

iptables_stream() {
  ${BINPATH}/iptables -A INPUT -p tcp --dport 8899 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p tcp --dport 8900 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p tcp --dport 8901 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p tcp --dport 8902 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p tcp --dport 55555 -j ACCEPT -w
  ${BINPATH}/iptables -A INPUT -p tcp --dport 55556 -j ACCEPT -w
}

# IPv6 only rules.
ip6tables_webserver_ctrl_if_setup() {
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 4000 -j ACCEPT -w
}

ip6tables_live555_setup() {
  ${BINPATH}/ip6tables -A INPUT -p udp --dport 6970 -j ACCEPT -w
  ${BINPATH}/ip6tables -A INPUT -p udp --dport 6971 -j ACCEPT -w
}

ip6tables_stream() {
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 8899 -j ACCEPT -w
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 8900 -j ACCEPT -w
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 8901 -j ACCEPT -w
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 8902 -j ACCEPT -w
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 55555 -j ACCEPT -w
  ${BINPATH}/ip6tables -A INPUT -p tcp --dport 55556 -j ACCEPT -w
}

# Install all rules.
for iptables in ip{,6}tables; do
  iptables_bin=${BINPATH}/${iptables}
  [ -x ${iptables_bin} ] || continue

  # Accept inbound traffic on 4000.
  ${iptables}_webserver_ctrl_if_setup

  # Accept inbound live555 traffic
  ${iptables}_live555_setup

  # Accept inbound
  ${iptables}_stream
done

setprop firewall.concam.init 1
